<?php
/*
Template Name: Articles Page
Template Post Type: page
*/

get_header();
?>
    <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div id="primary" class="content-area">
                        <main id="main" class="site-main">
                            <?php
                                while ( have_posts() ) :
                                    the_post();

                                    get_template_part( 'template-parts/home', 'page' );

                                    if ( comments_open() || get_comments_number() ) :
                                        comments_template();
                                    endif;

                                endwhile; // End of the loop.
                            ?>

                        </main>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php dynamic_sidebar( 'articles-sidebar' ); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h2>Betfinder</h2>
                    <si-lb-widget widget-id="HotbetFinder" language="en" client-id="default" provider="bet365" nopadding="true" width="100%" card="false"></si-lb-widget>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <h2>Flere artikler</h2>
                    <?php do_action( 'video_post_list', 'uncategorized', 4 ); ?>
                </div>
                <div class="col-md-4">
                    <aside class="widget-area">
                        <?php dynamic_sidebar( 'secondary-sidebar' ); ?>
                    </aside>
                </div>
            </div>
        </div>
<?php
get_footer();