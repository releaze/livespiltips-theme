<?php
/**
 * livespiltips functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package livespiltips
 */

if ( ! function_exists( 'livespiltips_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function livespiltips_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on livespiltips, use a find and replace
		 * to change 'livespiltips' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'livespiltips', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'livespiltips' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'livespiltips_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'livespiltips_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function livespiltips_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'livespiltips_content_width', 640 );
}
add_action( 'after_setup_theme', 'livespiltips_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function livespiltips_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'livespiltips' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'livespiltips' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'livespiltips_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function livespiltips_scripts() {
	wp_enqueue_style( 'livespiltips-style', get_stylesheet_uri() );

	wp_enqueue_script( 'livespiltips-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'livespiltips-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'livespiltips_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

//======================================================================================================================
add_action( 'wp_enqueue_scripts', 'livespiltips_style_theme' );
add_action( 'after_setup_theme', 'livespiltips_main_menu' );
add_action( 'after_setup_theme', 'livespiltips_image_size' );
add_action( 'widgets_init', 'livespiltips_register_sidebar' );
add_action( 'video_post_list', 'action_function', 10, 2 );

// Adds styles
function livespiltips_style_theme(){
    wp_enqueue_style( 'bootstrap_styles', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css', false, '4.4.1' );
    wp_enqueue_style( 'monserat_font', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&display=swap' );
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_style( 'default', get_template_directory_uri() . '/assets/css/default.css' );
}

function livespiltips_main_menu(){
    register_nav_menu( 'livespiltips_top_menu', 'Top Menu' );
}

function livespiltips_image_size(){
    add_image_size( 'post-thumb', 215, 135, true );
}

function livespiltips_register_sidebar(){
	register_sidebar( array(
		'name' => "Articles Sidebar",
		'id' => 'articles-sidebar',
		'description' => 'These widgets will be shown in the Articles Page.',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	) );

	register_sidebar( array(
        'name' => "Secondary Sidebar",
        'id' => 'secondary-sidebar',
        'description' => 'These widgets will be shown in the Secondary Sidebar.',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ) );
}

function action_function($category_name, $numberposts){
    $posts = get_posts( array(
        'numberposts'      => $numberposts,
        'category_name'    => $category_name,
        'post_type'        => 'post',
        'orderby'          => 'date',
        'order'            => 'DESC',
        'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
    ) );

    echo '<ul class="livespiltips-video-post-list">';

        foreach( $posts as $post ){
            setup_postdata($post);
            $title = isset( $post->post_title ) ? $post->post_title : '';
            $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumb' );
            $post_link = get_page_link( $post);

            printf('<li><figure>' .
                    '<img src="' . $thumbnail_src[0] . '" alt="">' .
                    '<figcaption><h2><a href=' . $post_link . '>' . $title . '</a></h2></figcaption>'
             . '</figure></li>');
        }

    echo '</ul>';

    wp_reset_postdata(); // сброс
}
