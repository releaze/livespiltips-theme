<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package livespiltips
 */

?>
	</div><!-- #content -->
	<footer id="colophon" class="site-footer">
        <div class="container">
            <div class="site-info">
                <div class="row">
                    <div class="col-md-8">
                        Privatlivspolitik
                    </div>
                    <div class="col-md-4">

                    </div>
                </div>
            </div>
        </div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
