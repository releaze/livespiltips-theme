<?php
/*
Template Name: Home Page
Template Post Type: page
*/

get_header();
?>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main">
                        <?php
                            while ( have_posts() ) :
                                the_post();

                                get_template_part( 'template-parts/home', 'page' );

                                if ( comments_open() || get_comments_number() ) :
                                    comments_template();
                                endif;

                            endwhile; // End of the loop.
                        ?>

                    </main>
                </div>
            </div>
            <div class="col-md-4">
                <?php get_sidebar(); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h2>Betfinder</h2>
                <si-lb-widget widget-id="HotbetFinder" language="en" client-id="default" provider="bet365" nopadding="true" width="100%" card="false"></si-lb-widget>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <h2>Flere artikler</h2>
                <?php do_action( 'video_post_list', 'uncategorized', 4 ); ?>
            </div>
            <div class="col-md-4">
                <aside class="widget-area">
                    <?php dynamic_sidebar( 'secondary-sidebar' ); ?>
                </aside>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h2>Info (SEO text)</h2>
                <p>ac ut consequat semper viverra nam libero justo laoreet sit amet cursus sit amet dictum sit amet justo donec enim diam vulputate ut pharetra sit amet aliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus mattis rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim sed faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu cursus euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras ornare arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce ut placerat orci nulla pellentesque dignissim enim sit amet venenatis urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt augue interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis tincidunt id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus vestibulum sed arcu non odio euismod lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum non consectetur a erat nam</p>
            </div>
        </div>
    </div>
<?php
get_footer();